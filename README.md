# Udon Audio Bank

A script for Udon that plays a random sound from a "bank" intermittently, with the interval randomly selected from a specified range.