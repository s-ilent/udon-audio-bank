using UdonSharp;
using UnityEngine;
using VRC.SDKBase;
using VRC.Udon;
using UdonToolkit;

public class AudioBank : UdonSharpBehaviour
{

	public float minTimeBetweenOneShots;
	public float maxTimeBetweenOneShots;

	public AudioClip[] clips;

	public AudioSource[] sources;

	private float GetNextTime()
	{
		return Random.Range(minTimeBetweenOneShots, maxTimeBetweenOneShots);
	}

    void Start()
    {
        // This script works by firing off a custom event after a random amount of time
        // Then the event will play a sound and schedule the next event
        // SendCustomEventDelayedSeconds
        float nextTime = GetNextTime();
        //Debug.Log("Playing next sample in "+ nextTime);
        SendCustomEventDelayedSeconds("_PlaySound", nextTime);
    }

    public void _PlaySound()
    {
    	int numSources = sources.Length;
    	int numClips = clips.Length;

    	int targetSource = Random.Range(0, numSources);
    	int targetClip = Random.Range(0, numClips);

    	sources[targetSource].PlayOneShot(clips[targetClip]);

        float nextTime = GetNextTime();
        //Debug.Log("Playing next sample in "+ nextTime);
        SendCustomEventDelayedSeconds("_PlaySound", nextTime);
    }
}
